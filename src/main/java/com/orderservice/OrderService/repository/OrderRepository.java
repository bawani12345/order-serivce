package com.orderservice.OrderService.repository;

import com.orderservice.OrderService.entitity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
    @Query("SELECT o FROM OrderEntity o Where o.user_id = ?1")
    List<OrderEntity> findOrdersByUserId(long id);
}
